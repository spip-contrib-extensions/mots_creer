<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'apercu_import'          => 'Voilà les mots clés qui vont être créés :',

	// B
	'bouton_creer'           => 'Créer les mots',

	// C
	'confirmer_import'       => 'Confirmer l\'import',
	'config_sans_mots_cles'  => 'Les mots clés ne sont pas activés dans la configuration des contenus du site.',

	// E
	'erreur_analyse'         => 'Erreur dans l\'analyse des mots clés',

	// G
	'groupe'                 => 'Dans le groupe',
	'groupe_pas_arborescent' => 'Ce groupe de mots n\'est pas arborescent',

	// M
	'mots'                   => 'Mots à créer',
	'mots_crees'             => 'Les mots ont été créées',
	'mots_explications'      => 'Un mot par ligne',
	'mots_explications_arbo' => 'Un mot par ligne.
<br>Deux espaces en début de ligne pour créer un sous mot (pour des mots arborescents)',
	'motscreer_titre'        => 'Créer des mots clés',

	// P
	'pas_autorise'           => 'Vous ne pouvez pas créer de mots dans ce groupe.',

);
