<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if( !defined('_ECRIRE_INC_VERSION') ){
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'motscreer_description' => 'Créer rapidement des mots clés à partir d\'une liste saisie ou copiée dans un champ texte',
	'motscreer_nom'         => 'Créer des mots clés',
	'motscreer_slogan'      => '',
);
